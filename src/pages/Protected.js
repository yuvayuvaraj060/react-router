import React from "react";
import { Redirect, Route, useLocation, useHistory } from "react-router-dom";
import { fakeAuth } from "./logIn.js";

const PrivateRoute = (props) => {
  const { component: Component, ...rest } = props;
  console.log(props, rest, useHistory(), useLocation());
  const location = useLocation();

  return (
    <Route {...rest}>
      {fakeAuth.isAuthenticated === true ? (
        <Component />
      ) : (
        <Redirect to={{ pathname: "/login", state: { from: location } }} />
      )}
    </Route>
  );
};

export default PrivateRoute;
