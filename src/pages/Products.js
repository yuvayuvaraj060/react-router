import React from "react";
import { Link, useRouteMatch, useParams, Route } from "react-router-dom";

function Product() {
  const { product } = useParams();
  return <div>{product}</div>;
}

const Products = () => {
  const { url, path } = useRouteMatch();
  return (
    <div>
      Products
      <div className="a">{url}</div>
      <div className="top">
        <ul>
          <li>
            <Link to={`${url}/veg`}>Veg</Link>
          </li>
          <li>
            <Link to={`${url}/nonVeg`}>Non Veg</Link>
          </li>
        </ul>
      </div>
      <div className="a">
        <Route path={`${path}/:product`}>
          <Product />
        </Route>
      </div>
    </div>
  );
};

export default Products;
