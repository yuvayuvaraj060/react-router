import { Route, Switch, Link } from "react-router-dom";
import "./App.css";
import Category from "./pages/Category.js";
import { Home } from "./pages/Home.js";
import Login from "./pages/logIn.js";
import Products from "./pages/Products.js";
import Protected from "./pages/Protected.js";

function App() {
  return (
    <div className="App">
      <div
        className="Nav__bar"
        style={{
          display: "flex",
          justifyContent: "space-around",
          width: "300px",
          height: "100px",
        }}
      >
        <Link to="/">Home</Link>
        <Link to={{pathname:"/category",state:{hello: "007"}}}>Category</Link>
        <Link to="/products">Products</Link>
      </div>
      <Switch>
        <Route exact path="/" component={Home} />
        <Route path="/category" component={Category} />
        <Route path="/login" component={Login} />
        <Protected path="/products" component={Products} />
      </Switch>
    </div>
  );
}

export default App;
